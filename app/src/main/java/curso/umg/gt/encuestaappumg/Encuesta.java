package curso.umg.gt.encuestaappumg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Encuesta extends AppCompatActivity {
    private EditText etEnc1,etEnc2,etEnc3,etEnc4,etEnc5;
    private ListView listaResp;
    private List<String> RespuestasList;
    private ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuesta);
        RespuestasList=new ArrayList<>();
        etEnc1=(EditText) findViewById(R.id.etEnc1);
        etEnc2=(EditText) findViewById(R.id.etEnc2);
        etEnc3=(EditText) findViewById(R.id.etEnc3);
        etEnc4=(EditText) findViewById(R.id.etEnc4);
        etEnc5=(EditText) findViewById(R.id.etEnc5);
        listaResp=(ListView) findViewById(R.id.lv1);
adapter= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,RespuestasList);
listaResp.setAdapter(adapter);
    }

    public void registrarEnc(View view){
        String value1="Nombre: "+etEnc1.getText().toString();
        String value2="Apellido"+etEnc2.getText().toString();
        String value3="Calificacion Catedraticos: "+etEnc3.getText().toString();
        String value4="Calificacion Aulas: "+etEnc4.getText().toString();
        String value5="Calificacion Mobiliario: "+etEnc5.getText().toString();


        RespuestasList.add(value1);
         RespuestasList.add(value2);
         RespuestasList.add(value3);
         RespuestasList.add(value4);
         RespuestasList.add(value5);
         adapter.notifyDataSetChanged();

        Toast notification = Toast.makeText(this, "Encuesta enviada con exito ", Toast.LENGTH_SHORT);
        notification.show();
    }
}
