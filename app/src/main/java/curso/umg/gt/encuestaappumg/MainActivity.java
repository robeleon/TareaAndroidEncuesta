package curso.umg.gt.encuestaappumg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText User,Pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        User =(EditText) findViewById(R.id.et1);
        Pass = (EditText) findViewById(R.id.et2);
    }
    public void ingresar(View view) {

        String user = User.getText().toString();
        String pass = Pass.getText().toString();

        if (user.equals("estudiante") && pass.equals("2017")) {

            Intent i = new Intent(this, Encuesta.class);
            startActivity(i);
        } else {
            Toast notification = Toast.makeText(this, "Credenciales invalidas ", Toast.LENGTH_SHORT);
            notification.show();
        }
    }
}
